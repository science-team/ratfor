# ratfor RPM spec file. Originally by Brian R. Gaeke <brg@dgate.org>, Mar 4 2001
Name: ratfor
Summary: Rational Fortran preprocessor
Version: 1.07
Release: 1
License: SUSE-Public-Domain
Source: ratfor-1.07.tar.gz
Group: Development/Languages/Fortran
BuildRoot: /var/tmp/%{name}-root
URL: http://www.dgate.org/ratfor/

%description
A preprocessor for the Rational Fortran language used in _Software
Tools_ and in various public domain codes.

%prep
%setup -q
%configure

%build
make %{?_smp_mflags}

%install
%make_install

%files
%defattr(-,root,root)
%doc BUGS README ratfor.1
%{_bindir}/ratfor
%{_mandir}/man1/ratfor.1.gz

%changelog
* Mon Jan 13 2025 Brian Gaeke <brg@dgate.org>
- 1.07-1
- Avoid buffer overrun in gettok->gtok when handling includes.
- Correct limitation on supported include file name length.

* Sun Dec  8 2024 Brian Gaeke <brg@dgate.org>
- 1.06-1
- Don't let gettok go off the end of its buffer when fetching include file name.
- Don't let gtok go off the end of lexstr when terminating a token that is too long.
- Document limitation on supported include file name length.

* Fri Apr  3 2020 Brian Gaeke <brg@dgate.org>
- 1.05-1
- Fix ordering of data statements when handling sequences of string directives 
- Warn if eos not defined when using string directive
- Added tests and documentation of known limitations

* Wed Feb 19 2020 Brian Gaeke <brg@dgate.org>
- 1.04-1
- String-related fixes
- Works in the presence of unsigned char
- Avoid gnbtok() walking off the beginning of infile[] (patch by Ole Streicher)
- Added tests and performed some code cleanup

* Tue Sep 15 2015 Brian Gaeke <brg@dgate.org>
- 1.03-1
- Incorporated Debian-related changes

* Tue Sep 15 2015 Brian Gaeke <brg@dgate.org>
- 1.02-1
- Package updated for modern RPM and Autoconf
- Warnings fixed

* Sun Mar  4 2001 Brian Gaeke <brg@dgate.org>
- 1.01-1
- Original packaged version
