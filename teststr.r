define(eos,-2)
define(lf,10)

# strings without user-specified sizes
string digone 'This ';
string digtwo "is a ";
string digthr test;
# strings with user-specified sizes that are exactly right
string digfou(12) "More string";
string digfiv(9) ' output ';
string digsix(8) follows;
# strings with user-specified sizes that are not exactly right
string digsev(6) 'short!--this part gets cut off';
string digeig(1000) 'long!';

call putstr(digone)
call putstr(digtwo)
call putstr(digthr)
call putchar(lf)
call putstr(digfou)
call putstr(digfiv)
call putstr(digsix)
call putchar(lf)
call putstr(digsev)
call putchar(lf)
call putstr(digeig)
call putchar(lf)
stop
end
