# Here we test including a file and making sure that a name defined inside the
# included file is usable in the including file.
# We have to arrange for the included file to be copied into the current
# working directory so that "make distcheck" works.
# ratfor does not seem to have the concept whereby a source file's directory
# is implicitly added to its include path.
include testin2.r
define B 400

call myfunc(A,B)
stop
end

subroutine myfunc(x,y)
integer x,y
write (*,100) 'myfunc called with x=', x, ' and y=', y
100 format (A,I3,A,I3)
end
