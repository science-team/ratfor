# Test that, when multiple string directives appear in sequence, we do not emit
# the data statements until the end of the sequence.
define eos -2
   subroutine swend (lab)
   integer lab

   string one "one"
   string two "two"
   if (lab == 1)
      call out(one)
   else
      call out(two)
   return
   end
