
subroutine foo(iarg)
integer iarg
switch(iarg) {
    case 0:
    write(6,*) "arg is zero"
    case 1:
    write(6,*) "arg is one"
    case 2,3:
    write(6,*) "arg is two or three"
    default:
    write(6,*) "arg is something else"
}
return
end

# main program
call foo(4)
call foo(3)
call foo(2)
call foo(1)
call foo(0)
stop
end
